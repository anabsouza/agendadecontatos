package br.com.itau;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner scanner = new Scanner(System.in);
        int qtdPessoas = scanner.nextInt();
        Agenda agenda = new Agenda();

        for(int ct = 1; ct < qtdPessoas; ct++){
            Scanner scanner1 = new Scanner(System.in);
            System.out.println("Digite o nome:");
            String nome = scanner1.nextLine();
            System.out.println("Digite o telefone:");
            Integer telefone = scanner1.nextInt();
            System.out.println("Digite o email:");
            String email = scanner1.nextLine();
            Pessoas pessoas = new Pessoas(
                    nome,
                    telefone,
                    email
            );

            agenda.AdicionarContato(pessoas);
        }

        }
    }


