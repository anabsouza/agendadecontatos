package br.com.itau;

public class Pessoas {
        private String nome;
        private int numtelefone;
        private String email;

        public Pessoas(String nome, Integer numtelefone, String email) {
            this.nome = nome;
            this.numtelefone = numtelefone;
            this.email = email;
        }

        public String getNome() {
            return nome;
        }

        public void setNome(String nome) {
            this.nome = nome;
        }

        public int getNumtelefone() {
            return numtelefone;
        }

        public void  setNumtelefone(int numtelefone){
            this.numtelefone = numtelefone;
        }

        public String getEmail() {
            return email;
        }
    }


